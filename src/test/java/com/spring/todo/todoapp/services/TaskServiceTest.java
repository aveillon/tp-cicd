package com.spring.todo.todoapp.services;

import com.spring.todo.todoapp.models.dto.TaskRequest;
import com.spring.todo.todoapp.models.entity.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.lang.RuntimeException

import javax.transaction.Transactional;

import static com.google.common.truth.Truth.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TaskServiceTest {

    @Autowired
    private TaskService taskService;

    @Test
    @Transactional
    public void testCreateTaskOK() {
        TaskRequest taskRequestOk = new TaskRequest();
        taskRequestOk.setCompleted(false);
        taskRequestOk.setDescription("Ma tache");

        Task taskOk = taskService.create(taskRequestOk);
        assertThat(taskOk).isNotNull();
    }

    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testCreateTaskError() {
        TaskRequest taskRequestError = new TaskRequest();
        taskRequestError.setCompleted(false);

        taskService.create(taskRequestError);
    }
}

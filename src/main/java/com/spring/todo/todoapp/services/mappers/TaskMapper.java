package com.spring.todo.todoapp.services.mappers;

import com.spring.todo.todoapp.models.dto.TaskResponse;
import com.spring.todo.todoapp.models.entity.Task;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskMapper {

    @Named(value = "toTaskSimpleResponse")
    TaskResponse toSimpleResponse(Task task);

    @Named("toTaskSimpleResponseList")
    @IterableMapping(qualifiedByName = "toTaskSimpleResponse")
    List<TaskResponse> toSimpleResponseList(List<Task> tasks);
}
